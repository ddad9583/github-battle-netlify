import PROFILE from "./profile.json";
import PERSONAL_REPOS from "./personalRepositories.json";
import POPULAR_REPOS from "./popularRepos.json";

/*

  TODO: Enter your own Github client id and secret id below

  1. Visit Github.com
  2. Visit User Settings (https://github.com/settings/profile)
  3. Select "Developer Settings"
  4. Select "Oauth Apps"
  5. Select "New Oauth App"
  6. Enter "http://localhost:8080" for homepage & callback URL
  7. Enter your Client ID and Secret ID below

 */
const GITHUB_CLIENT_ID = "b3e325cb8e1753546cd7";
const GITHUB_SECRET_ID = "c6620cba9a36141f54a57f740de1f30b4d39aaae";
const GITHUB_PERSONAL_ACCESS_TOKEN = "token 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"

const defaultParams = `?client_id=${GITHUB_CLIENT_ID}&client_secret=${GITHUB_SECRET_ID}`;

// NOTE: Toggle this value to use mock data.
const USE_MOCK_DATA = false;

function getErrorMsg(message, username) {
  if (message === "Not Found") {
    return `"${username}"는 존재하지 않는 사용자입니다`;
  }

  return message;
}

async function request(uri) {
  const response = await fetch(uri, {
    headers: {
      Authorization: GITHUB_PERSONAL_ACCESS_TOKEN,
    }
  });

  return response;
}

// TODO: Refactor with `async/await`
export async function getProfile(username) {
  if (USE_MOCK_DATA) {
    return new Promise(function (resolve) {
      resolve(PROFILE);
    });
  }

  const response = await request(`https://api.github.com/users/${username}${defaultParams}`);
  const profile = await response.json();

  for (const key of Object.keys(profile)) {
    if (profile[key] === null) {
      profile[key] = "N/A";
    }
  }

  console.log(`PROFILE:::`, JSON.stringify(profile));

  if (profile.message) {
    throw new Error(getErrorMsg(profile.message, username));
  }

  return profile;
}

// TODO: Refactor with `async/await`
async function getRepos(username) {
  if (USE_MOCK_DATA) {
    return new Promise(function (resolve) {
      resolve(PERSONAL_REPOS);
    });
  }

  const response = await request(`https://api.github.com/users/${username}/repos${defaultParams}&per_page=100`);
  const repos = await response.json();

  console.log(`REPOSE:::`, JSON.stringify(repos));

  if (repos.message) {
    throw new Error(getErrorMsg(repos.message, username));
  }
  return repos;

}

function getStarCount(repos) {
  return repos.reduce(
    (count, { stargazers_count }) => count + stargazers_count,
    0
  );
}

function calculateScore(followers, repos) {
  return followers * 3 + getStarCount(repos);
}

async function getUserData(player) {
  const profile = await getProfile(player);
  const repos = await getRepos(player);

  return {
    profile,
    score: calculateScore(profile.followers, repos),
  };
}

function sortPlayers(players) {
  const playersHasError = players[0].status === "rejected" || players[1].status === "rejected";

  if (playersHasError) {
    const errorMessage = players.reduce((acc, cur) => {
      if (cur.status === "rejected") {
        return acc + cur.reason.message + ".\n";
      }

      return acc;
    }, "");
    throw new Error(errorMessage);
  }

  players.sort((a, b) => b.value.score - a.value.score);

  return [players[0].value, players[1].value];
}

export async function fetchBattleStat([player1, player2]) {
  const playerOne = getUserData(player1);
  const playerTwo = getUserData(player2);

  return sortPlayers(await Promise.allSettled([playerOne, playerTwo]));
}

export async function fetchPopularRepos(language) {
  if (USE_MOCK_DATA) {
    return new Promise(function (resolve) {
      if (language === "All") return resolve(POPULAR_REPOS);

      resolve(POPULAR_REPOS.filter((repo) => repo.language === language));
    });
  }

  const endpoint = window.encodeURI(
    `https://api.github.com/search/repositories${defaultParams}&q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories`
  );

  const res = await fetch(endpoint, {
    headers: {
      Authorization: GITHUB_PERSONAL_ACCESS_TOKEN,
    }
  });
  const { items } = await res.json();

  return items;
}
