import React from "react";
import { render, screen, act } from "@testing-library/react";
import Loading from "../components/Loading";

afterEach(() => {
  jest.useRealTimers();
});

test("Loading component should show text prop", () => {
  const TEXT = "hello world";

  render(<Loading text={TEXT} />);
  expect(screen.getByText(TEXT)).toBeInTheDocument();
});

test("Loading component should rerender with updated text at set speed.", () => {
  const TEXT = "testing";
  const SPEED = 300;
  const TESTCOUNT = 100;
  const additionalStringsList = ["", ".", "..", "..."];
  const listLength = additionalStringsList.length;

  jest.useFakeTimers();

  render(<Loading text={TEXT} speed={SPEED} />);

  for (let i = 0; i < TESTCOUNT; i++) {
    expect(screen.getByText(TEXT + additionalStringsList[i % listLength]))
      .toBeInTheDocument();
    expect(() => screen.getByText(TEXT + additionalStringsList[(i + 1) % listLength]))
      .toThrow("Unable to find an element with the text");
    expect(() => screen.getByText(TEXT + additionalStringsList[(i + 2) % listLength]))
      .toThrow("Unable to find an element with the text");
    expect(() => screen.getByText(TEXT + additionalStringsList[(i + 3) % listLength]))
      .toThrow("Unable to find an element with the text");
    act(() => jest.advanceTimersByTime(SPEED));
  }
});
