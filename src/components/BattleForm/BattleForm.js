import React, { Fragment, useEffect, useState } from "react";
import "./styles.css";

export default function BattleForm({ onSubmit }) {
  const [enteredContestant, setEnteredContestant] = useState({
    contestant1: "",
    contestant2: "",
  });
  const [isInputsValid, setIsInputsValid] = useState(false);

  useEffect(() => {
    if (
      enteredContestant.contestant1.trim().length > 0
      && enteredContestant.contestant2.trim().length > 0
    ) {
      setIsInputsValid(true);
      return;
    }

    setIsInputsValid(false);
  }, [enteredContestant]);

  function handleUserInput(event) {
    if (event.target.id === "contestant1") {
      setEnteredContestant((prevState) => ({
        ...prevState,
        contestant1: event.target.value,
      }));
      return;
    }
    setEnteredContestant((prevState) => ({
      ...prevState,
      contestant2: event.target.value,
    }));
  }

  function handleFormSubmission(event) {
    event.preventDefault();
    onSubmit(event);
    setEnteredContestant({ contestant1: "", contestant2: "" });
  }

  return (
    <Fragment>
      <form id="battle-form" onSubmit={handleFormSubmission}>
        <div className="input-block">
          <label htmlFor="contestant1">Player1</label>
          <input
            id="contestant1"
            type="text"
            value={enteredContestant.contestant1}
            onChange={handleUserInput}
            placeholder="Please enter Github username."
          />
        </div>
        <div className="vsBox">VS</div>
        <div className="input-block">
          <label htmlFor="contestant2">Player2</label>
          <input
            id="contestant2"
            type="text"
            value={enteredContestant.contestant2}
            onChange={handleUserInput}
            placeholder="Please enter Github username."
          />
        </div>
      </form>
      <button
        className="form-button"
        form="battle-form"
        disabled={!isInputsValid}
      >
        Start Battle!
      </button>
    </Fragment>
  );
}
