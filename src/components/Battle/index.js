import React, { Fragment, useState, useEffect } from "react";
import BattleForm from "../BattleForm/BattleForm";
import { fetchBattleStat } from "../../utils/api";
import Loading from "../Loading/index";
import BattleResult from "../BattleResult";
import Modal from "../Modal";

export default function Battle({ data, onFetchData }) {
  const [isLoading, setIsLoading] = useState(false);
  const [formSubmission, setFormSubmission] = useState({});
  const [error, setError] = useState({ showErrorModal: false, message: "" });
  const isdataEmpty = data.length === 0 ? true : false;
  let isMount = true;

  useEffect(() => {
    async function getBattleStat() {
      try {
        if (isLoading) {
          const response = await fetchBattleStat([
            formSubmission.contestant1,
            formSubmission.contestant2,
          ]);

          if (!isMount) {
            return;
          }

          onFetchData(response);
        }
      } catch (error) {
        onFetchData([]);
        setError({ showErrorModal: true, message: error.message });
      }
      setIsLoading(false);
    }

    getBattleStat();

    return () => isMount = false;
  }, [isLoading, formSubmission, onFetchData]);

  function handleFormSubmission(event) {
    const contestant1 = event.target[0].value;
    const contestant2 = event.target[1].value;

    setFormSubmission({ contestant1, contestant2 });
    setIsLoading(true);
  }

  function handleModalClick() {
    setError({ showErrorModal: false, message: "" });
  }

  return (
    <Fragment>
      <h1 className="center-text">This is Battle!</h1>
      <BattleForm onSubmit={handleFormSubmission} />
      {isLoading && <Loading />}
      {error.showErrorModal && <Modal message={error.message} onClick={handleModalClick} />}
      {!isLoading && !isdataEmpty && <BattleResult data={data} />}
    </Fragment>
  );
}
