import React from "react";
import "./styles.css";

export default function BattleResult({ data }) {
  const contestantsList = data.map((contestant, index) => {
    const {
      id,
      avatar_url,
      login,
      name,
      location,
      followers,
      following,
      public_repos,
    } = contestant.profile;
    let resultText = "LOSER";
    let resultClass = "loser";

    if (index === 0) {
      resultText = "WINNER";
      resultClass = "winner";
    }

    return (
      <ul key={id} className={`contestant ${resultClass}`}>
        <li className={resultClass}>{resultText}</li>
        <img src={avatar_url} alt={login}></img>
        <li>Score<span>:</span> {contestant.score}</li>
        <li>Name<span>:</span> {name}</li>
        <li>Github Username<span>:</span> {login}</li>
        <li>Location<span>:</span> {location}</li>
        <li>Followers Count<span>:</span> {followers}</li>
        <li>Following Count<span>:</span> {following}</li>
        <li>Repository Count<span>:</span> {public_repos}</li>
      </ul>
    );
  });

  return (
    <div className={"result-container"}>
      {contestantsList}
    </div>
  );
}
