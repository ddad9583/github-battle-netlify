import { createPortal } from "react-dom";
import React, { Fragment } from "react";
import "./styles.css";

export default function Modal({ message, onClick }) {
  const $rootElment = document.getElementById("root");

  return createPortal(
    <Fragment>
      <div className="backdrop" onClick={onClick}></div>
      <div className="modal">
        {message}
        <button onClick={onClick} className="modal-button">Close</button>
      </div>
    </Fragment>,
    $rootElment
  )
}
